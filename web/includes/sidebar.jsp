<aside class="menu" style="min-height:600px;">
    <p class="menu-label">
        Menu
    </p>
    <ul class="menu-list">


        <%
            if (session.getAttribute("role") != null) {
                if (session.getAttribute("role").equals("Kasir")) {
        %>
        <li><a href="/inventori/stok-obat">Stok</a></li>
        <li><a href="/inventori/penjualan">Penjualan</a></li>
        
        <%
                }
            }
        %>

        <%
            if (session.getAttribute("role") != null) {
                if (session.getAttribute("role").equals("Logistik")) {
        %>
        <li><a href="/inventori/obat">Obat</a></li>
        <li><a href="/inventori/pemesanan">Pemesanan</a></li>
        <li><a href="/inventori/stok-obat">Stok</a></li>
        <li><a>Kelola Persediaan</a>
            <ul>
                <li><a href="/inventori/obatmasuk">Obat Masuk</a></li>
            </ul>
        </li>
        <%
                }
            }
        %>

        <%
            if (session.getAttribute("role") != null) {
                if (session.getAttribute("role").equals("Keuangan")) {
        %>
        <li><a href="/inventori/pembelian">Pembelian</a></li>
        <li><a href="/inventori/index.jsp?halaman=data_akun">Akun</a></li>
        <li><a href="/inventori/index.jsp?halaman=jurnal_umum">Jurnal</a></li>
        <li><a href="/inventori/index.jsp?halaman=informasi_jurnal">Data Jurnal</a></li>
    </ul>
    <%
            }
        }
    %>


    <%
        if (session.getAttribute("role") != null) {
            if (session.getAttribute("role").equals("Pimpinan")) {
    %>
    <p class="menu-label">
        Laporan
    </p>

    <ul class="menu-list">
        <li><a href="/inventori/report-akun" target="_blank">Akun</a></li>
        <li><a href="/inventori/index.jsp?halaman=laporan_stok_harian">Laporan Persediaan Harian</a></li>
        <li><a href="/inventori/index.jsp?halaman=laporan_stok_bulanan">Laporan Persediaan Bulanan</a></li>
        <li><a href="/inventori/index.jsp?halaman=laporan_stok_tahunan">Laporan Persediaan Tahunan</a></li>

        <li><a href="/inventori/index.jsp?halaman=laporan_jurnal_harian">Laporan Jurnal Harian</a></li>
        <li><a href="/inventori/index.jsp?halaman=laporan_jurnal_bulanan">Laporan Jurnal Bulanan</a></li>
        <li><a href="/inventori/index.jsp?halaman=laporan_jurnal_tahunan">Laporan Jurnal Tahunan</a></li>

    </ul>
    <%
            }
        }
    %>
    
    <%
        if (session.getAttribute("role") != null) {
            if (session.getAttribute("role").equals("Admin")) {
    %>
    <ul class="menu-list">
        <li><a href="/inventori/user">User</a></li>

    </ul>
    <%
            }
        }
    %>
    <hr>
    <ul class="menu-list">
        <li><a href="/inventori/login?aksi=logout">Logout</a></li>

    </ul>


</aside>