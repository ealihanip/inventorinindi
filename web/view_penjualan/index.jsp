

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Pembelian</p>

        </div>

        <div class="card-content">

            <a href="penjualan?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>
                        
                        <td>
                            No Register
                        </td>

                        <td>
                            Tanggal
                        </td>
                       
                        
                        <td>
                            Nama Pasien
                        </td>

                        <td>
                            Alamat
                        </td>

                        
                        <td>
                            Poliklinik
                        </td>
                        
                        <td>
                            Dokter
                        </td>

                        <td>
                            Bayar
                        </td>

                        <td>
                            Aksi
                        </td>

                        
                    </tr>  
                </thead>

                </tbody>
                <c:forEach items="${penjualan}" var="penjualan">
                    <tr>
                        <td>
                            <c:out value="${penjualan.id}" />
                        </td>
                        
                        <td>
                            <c:out value="${penjualan.no_register}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.tanggal}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.nama_pasien}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.alamat}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.poliklinik}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.dokter}" />
                        </td>

                        <td>
                            <c:out value="${penjualan.bayar}" />
                        </td>

                        <td>
                             <a class="button has-background-light" href="penjualan?aksi=detailpenjualan&id=${penjualan.id}">List Obat</a>
                        </td>

                        
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>