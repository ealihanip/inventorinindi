<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p></p>

        </div>

        <div class="card-content">
            <form method="post" action="penjualan?aksi=store">
                <div class="columns">
                    <div class="column">

                        <div class="field">
                            <label class="label">No Register</label>
                            <div class="control">
                                <input class="input" name="no_register" type="text" placeholder="No Registrasi">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Tanggal</label>
                            <div class="control">
                                <input class="input tanggal" name="tanggal" type="text" placeholder="Tanggal">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Nama Pasien</label>
                            <div class="control">
                                <input class="input" name="nama_pasien" type="text" placeholder="Nama Pasien">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Alamat</label>
                            <div class="control">
                                <input class="input" name="alamat" type="text" placeholder="alamat">
                            </div>
                        </div>


                    </div>
                    <div class="column">

                        <div class="field">
                            <label class="label">Poliklinik</label>
                            <div class="control">
                                <input class="input" name="poliklinik" type="text" placeholder="Poliklinik">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Dokter</label>
                            <div class="control">
                                <input class="input" name="dokter" type="text" placeholder="Dokter">
                            </div>
                        </div>

                       

                    </div>

                </div>
                <hr>
                <div class="columns has-background-light is-multiply">
                    <div class="column is-3">
                        <div class="field">
                            <label class="label">Obat</label>
                            <div class="control">
                                <div class="select is-primary">
                                    <select name="obat_id" id='obat'>

                                        <c:forEach items="${obat}" var="obat">

                                            <option value="<c:out value="${obat.id}" />" >
                                            <c:out value="${obat.nama}" />
                                            </option>


                                        </c:forEach>


                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Jumlah</label>
                            <div class="control">
                                <input class="input" name="jumlah" type="text" placeholder="Jumlah" value='0' id='jumlah'>
                            </div>
                        </div>

                        <a class="button is-success" id='simpan-obat'>Tambah</a>
                    </div>

                </div>

                <div class="columns has-background-light is-multiply">
                    <div class="column is-full">


                        <div class="columns">
                            <div class="column is-2">ID OBAT</div>
                            <div class="column is-2">Nama Obat</div>
                            <div class="column is-2">Jumlah</div>
                            <div class="column is-2">Satuan</div>
                            <div class="column is-2">Harga Satuan</div>
                            <div class="column is-2">Total Harga</div>
                        </div>

                        <div id='input-obat'>
                        </div>

                         
                    </div>
                        

                </div>


                <div class="columns">
                    
                    <div class="column is-6">
                        <div class="field is-grouped">
                            <div class="control">
                                <input type="submit" class="button is-link" value="submit">
                            </div>
                            <div class="control">
                                <a href="penjualan" class="button">Cancel</a>
                            </div>
                        </div>

                    </div>


                    <div class="column is-6">
                        <div class="columns">
                            
                            <div class="column is-3">Total</div>
                            <div class="column is-6"><input type="hidden" name='totalbayar' id='totalbayar' value='0'><div id="totalharga">0</div></div>
                        </div>

                        <div class="columns">
                            
                            <div class="column is-3">Bayar</div>
                            <div class="column is-6">
                                <input type="text" class="input" id='bayar'> 
                                
                            </div>
                            <div class="column is-3"><a class="button is-black" onclick="bayar()">Bayar</a></div>
                        </div>

                        <div class="columns">
                            
                            <div class="column is-3">Kembalian</div>
                            <div class="column is-6"><div id="Kembalian">0</div></div>
                        </div>

                    </div>
                    


                </div>

                    





                
            </form>



            

            <script>

                $(document).ready(function () {
                    $("#tambah").click(function () {
                        $(".modal").addClass("is-active");
                    });

                    $("#modal-close").click(function () {
                        $(".modal").removeClass("is-active");
                    });

                    $("#simpan-obat").click(function () {
                        $(".modal").removeClass("is-active");

                        var obat = $('#obat').children("option:selected").val();
                        tambahobat(obat);
                    });
                });


               
            </script>

            <script>


                
                function tambahobat(id){

                    $.ajax({
                        type: "POST",
                        url: '?aksi=getdataobat&id='+id,
                        data: "check",
                        success: function(response){
                            
                            var jumlah=$('#jumlah').val();
                            var totalharga=response.harga_jual*jumlah;
                           
                    
                            var fieldHTML = '<div class="columns" id="input-obat">'
                                    +'<div class="column is-2"><input type="hidden" name="id[]" value="'+response.id+'">'+response.id+'</div>'
                                    +'<div class="column is-2"><input type="hidden" name="nama[]" value="'+response.nama+'">'+response.nama+'</div>'
                                    +'<div class="column is-2"><input type="hidden" name="jumlah[]" value="'+jumlah+'">'+jumlah+'</div>'
                                    +'<div class="column is-2">'+response.satuan+'</div>'
                                    +'<div class="column is-2">'+response.harga_jual+'</div>'
                                    +'<div class="column is-2">'+totalharga+'</div>'
                                +'</div>'; //New input field html 
                            
                           
                                
                            
                            $('#input-obat').append(fieldHTML); //Add field html

                            var totalhargapasien=document.getElementById("totalharga").innerHTML;

                            totalhargapasien=parseInt(totalhargapasien)+totalharga;
                            
                            $("#totalharga").html(totalhargapasien);
                            $("#totalbayar").val(totalhargapasien);

                             
                        }
                    });

                }
            </script>

            <script type="text/javascript">


                function bayar(){
                    var totalhargapasien=document.getElementById("totalharga").innerHTML;
                    var bayar=$( "#bayar" ).val();

                    var kembalian=bayar-parseInt(totalhargapasien);
                    $("#Kembalian").html(kembalian);
                }
                // $(document).ready(function () {
                    

                //     //Once remove button is clicked
                //     $(wrapper).on('click', '.remove_button', function (e) {
                //         e.preventDefault();
                //         $(this).parent('div').remove(); //Remove field html
                //         x--; //Decrement field counter
                //     });
                // });
            </script>

        </div>

    </div>

</div>