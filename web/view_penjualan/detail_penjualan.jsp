

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Detail Pembelian Obat</p>

        </div>

        <div class="card-content">

            
            <a href="penjualan" class="button has-background-primary">Kembali</a>
            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>
                        
                        <td>
                            ID Obat
                        </td>

                        <td>
                            Nama Obat
                        </td>
                       
                        
                        <td>
                            Jumlah
                        </td>

                        

                        
                    </tr>  
                </thead>

                </tbody>
                <c:forEach items="${detailpenjualan}" var="detailpenjualan">
                    <tr>
                        <td>
                            <c:out value="${detailpenjualan.id}" />
                        </td>
                        
                        <td>
                            <c:out value="${detailpenjualan.obat_id}" />
                        </td>

                        <td>
                            <c:out value="${detailpenjualan.nama_obat}" />
                        </td>

                        <td>
                            <c:out value="${detailpenjualan.jumlah}" />
                        </td>

        

                        
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>