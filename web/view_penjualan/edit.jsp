<div class="column is-9">

        <c:choose>
            <c:when test="${status=='success'}">
                <div class="notification is-primary">
                    <button class="delete"></button>
    
                    Berhasil
    
                </div>
            </c:when>
            <c:otherwise>
    
            </c:otherwise>
        </c:choose>
    
    
        <div class="card">
    
            <div class="card-header-title">
    
                <p>Edit Pembelian</p>
    
            </div>
    
            <div class="card-content">
    
    
    
                <form method="post" action="pembelian?aksi=update&id=${pembelian.id}">
                    <div class="field">
                        <label class="label">Note Pembelian</label>
                        <div class="control">
                            <input class="input" name="nota_pembelian" type="text" placeholder="Nota Pembelian" value="${pembelian.nota_pembelian}">
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label">Nama Obat</label>
                        <div class="control">
                            <div class="select is-primary">
                                <select name="obat_id">
    
                                    <c:forEach items="${obat}" var="obat">
    
                                        <option value="<c:out value="${obat.id}"/>" <c:choose>
                                                <c:when test="${pembelian.obat_id==obat.id}">
                                                    selected
                                                </c:when>
                                                <c:otherwise>
                                        
                                                </c:otherwise>
                                            </c:choose>>
                                        <c:out value="${obat.nama}" />
                                        </option>
    
    
                                    </c:forEach>
    
    
                                </select>
                            </div>
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label">Jumlah</label>
                        <div class="control">
                            <input class="input" name="jumlah" type="text" placeholder="Jumlah" value="${pembelian.jumlah}">
                        </div>
                    </div>
                    <div class="field">
                            <label class="label">Nominal</label>
                            <div class="control">
                                <input class="input" name="nominal" type="text" placeholder="nominal" value="${pembelian.nominal}">
                            </div>
                        </div>
    
                    <div class="field">
                        <label class="label">Tanggal</label>
                        <div class="control">
                            <input class="input tanggal" name="tanggal" type="text" placeholder="Tanggal" value="${pembelian.tanggal}">
                        </div>
                    </div>
    
                    <div class="field is-grouped">
                        <div class="control">
                            <input type="submit" class="button is-link" value="submit">
                        </div>
                        <div class="control">
                            <a href="pembelian" class="button">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
    
        </div>
    
    </div>