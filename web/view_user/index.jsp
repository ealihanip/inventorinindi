<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>User</p>

        </div>

        <div class="card-content">

            <a href="user?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>

                        <td>
                            Username
                        </td>

                        <td>
                            Password
                        </td>

                        <td>
                            Role
                        </td>

                        <td>
                            aksi
                        </td>
                    </tr>
                </thead>

                </tbody>
                <c:forEach items="${user}" var="user">
                    <tr>
                        <td>
                            <c:out value="${user.id}" />
                        </td>
                        <td>
                            <c:out value="${user.username}" />
                        </td>
                        <td>
                            <c:out value="${user.password}" />
                        </td>
                        <td>
                            <c:out value="${user.role}" />
                        </td>
                        <td>
                            <a class='button has-background-warning' href="user?aksi=edit&id=<c:out value="${user.id}" />">Edit</a>
                            <a class='button has-background-danger' href="user?aksi=delete&id=<c:out value="${user.id}" />">hapus</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>