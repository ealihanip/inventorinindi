<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p>Tambah User</p>

        </div>

        <div class="card-content">

            <form method="post" action="user?aksi=store">
                <div class="field">
                    <label class="label">Username</label>
                    <div class="control">
                        <input class="input" name="username" type="text" placeholder="Username">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Username</label>

                    <div class="select">
                        <select name="role">
                            <option value="Admin">Admin</option>
                            <option value="Logistik">Logistik</option>
                            <option value="Keuangan">Keuangan</option>
                            <option value="Pimpinan">Pimpinan</option>
                            <option value="Kasir">Kasir</option>

                        </select>
                    </div>

                </div>


                <div class="field">
                    <label class="label">Password</label>
                    <div class="control">
                        <input class="input" name="password" type="text" placeholder="Password">
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <input type="submit" class="button is-link" value="submit">
                    </div>
                    <div class="control">
                        <a href="user" class="button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>