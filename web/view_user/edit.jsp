<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p>Edit User</p>

        </div>

        <div class="card-content">

            <form method="post" action="user?aksi=update&id=${user.id}">
                <div class="field">
                    <label class="label">Username</label>
                    <div class="control">
                        <input class="input" name="username" type="text" placeholder="Username" value="${user.username}">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Role</label>

                    <div class="select">
                        <select name="role">
                            <option value="Admin" <c:choose>
                                <c:when test="${user.role=='Admin'}">
                                    selected
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                                </c:choose>
                                >Admin
                            </option>
                            <option value="Logistik" <c:choose>
                                <c:when test="${user.role=='Logistik'}">
                                    selected
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                                </c:choose>
                                >Logistik
                            </option>
                            <option value="Keuangan" <c:choose>
                                <c:when test="${user.role=='Keuangan'}">
                                    selected
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                                </c:choose>
                                >Keuangan
                            </option>
                            <option value="Pimpinan" <c:choose>
                                <c:when test="${user.role=='Pimpinan'}">
                                    selected
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                                </c:choose>
                                >Pimpinan
                            </option>

                            <option value="Pimpinan" <c:choose>
                                <c:when test="${user.role=='Kasir'}">
                                    selected
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                                </c:choose>
                                >Kasir
                            </option>

                        </select>
                    </div>

                </div>


                <div class="field">
                    <label class="label">Password</label>
                    <div class="control">
                        <input class="input" name="password" type="text" placeholder="Password" value="${user.password}">
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <input type="submit" class="button is-link" value="submit">
                    </div>
                    <div class="control">
                        <a href="user" class="button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>