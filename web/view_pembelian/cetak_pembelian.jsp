<%
    
    if (session.getAttribute("role") == null) {

        response.sendRedirect("login");
    }
    
%>


<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


</head>

<body class="">

    <div id='body'>
        
        
        <div class="container">

            <p class="h1">Pembelian Obat</p>
            <hr>
            <div class="row">

                <div class="col-md-2">

                    No Pembelian

                </div>

                <div class="col-md-10">
                    : ${pembelian.id}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Supplier

                </div>

                <div class="col-md-10">
                    : ${pembelian.supplier}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Total Tagihan

                </div>

                <div class="col-md-10">
                    : ${pembelian.total_tagihan}
                </div>

            </div>


            <div class="row">

                <div class="col-md-2">

                    Keterangan

                </div>

                <div class="col-md-10">
                    : ${pembelian.keterangan}
                </div>

            </div>


            <div class="row">

                <div class="col-md-2">

                    Jumlah Bayar

                </div>

                <div class="col-md-10">
                    : ${pembelian.jumlah_bayar}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Tanggal

                </div>

                <div class="col-md-10">
                    : ${pembelian.tanggal}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Kode Obat

                </div>

                <div class="col-md-10">
                    : ${pembelian.obat_id}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Nama Obat

                </div>

                <div class="col-md-10">
                    : ${pembelian.nama_obat}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Jumlah

                </div>

                <div class="col-md-10">
                    : ${pembelian.jumlah}
                </div>

            </div>

            

        </div>




    </div>


    <script>

        window.print();
        
    </script>
</body>

</html>