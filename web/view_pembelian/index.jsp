

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Pembelian</p>

        </div>

        <div class="card-content">

            <a href="pembelian?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            ID Pembelian
                        </td>
                        
                        <td>
                            ID Pemesanan
                        </td>

                        <td>
                            ID Obat
                        </td>
                       
                        
                        <td>
                            Nama Obat
                        </td>

                        
                        <td>
                            Jumlah
                        </td>
                        
                        <td>
                            Tanggal
                        </td>

                        <td>
                            Total Tagihan
                        </td>

                        <td>
                            Jumlah Bayar
                        </td>
                        <td>
                            aksi
                        </td>
                    </tr>  
                </thead>

                </tbody>
                <c:forEach items="${pembelian}" var="pembelian">
                    <tr>
                        <td>
                            <c:out value="${pembelian.id}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.pemesanan_id}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.obat_id}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.nama_obat}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.jumlah}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.tanggal}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.total_tagihan}" />
                        </td>

                        <td>
                            <c:out value="${pembelian.jumlah_bayar}" />
                        </td>
                        
                        <td>
                            <a class='button has-background-warning' target="_blank" href="pembelian?aksi=cetak&id=<c:out value="${pembelian.id}"/>">cetak</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>