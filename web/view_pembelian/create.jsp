<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p>Tambah Pembelian</p>

        </div>

        <div class="card-content">



            <form method="post" action="pembelian?aksi=store">
                
                <div class="columns">
                    <div class="column is-6">
                        <div class="field">
                            <label class="label">No pemesanan</label>
                            <div class="control">
                                <input class="input" id="no_pemesanan" name="no_pemesanan" type="text" placeholder="No Pemesanan" onchange="getdatapemesanan()">
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Supplier</label>
                            <div class="control">
                                <input class="input" id="supplier" name="supplier" type="text" placeholder="supplier">
                            </div>
                        </div>
    
                        
                        
    
                        <div class="field">
                            <label class="label">Total Tagihan</label>
                            <div class="control">
                                <input class="input" name="total_tagihan" type="text" placeholder="Tanggal Tagihan">
                            </div>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="field">
                            <label class="label">Tanggal</label>
                            <div class="control">
                                <input class="input tanggal" name="tanggal" type="text" placeholder="Tanggal Tagihan">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Keterangan</label>
                            <div class="select" >
                                <select name='keterangan'>
                                    <option value="cash">Cash</option>
                                    <option value="Kredit">Kredit</option>
                                </select>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Jumlah Bayar</label>
                            <div class="control">
                                <input class="input" name="jumlah_bayar" type="text" placeholder="Jumlah Bayar">
                            </div>
                        </div>
                    </div>
                    
                </div>


                

                <div class="field is-grouped">
                    <div class="control">
                        <input type="submit" class="button is-link" disabled value="Save" id='kirim'>
                    </div>
                    <div class="control">
                        <a href="pembelian" class="button">Cancel</a>
                    </div>
                </div>
            </form>

            <br>

            <div class="columns">
                
                <div class="column">
                    <div class="card">

                        <div class="card-content" style="width:100%">
                            <div class="columns">

                                <div class="column is-4">
                                    ID Obat
                                </div>

                                <div class="column is-4">
                                    Nama Obat
                                </div>

                                <div class="column is-4">
                                    Jumlah 
                                </div>
                                
                            </div>

                            <div id="data-obat">

                                
                                
                            </div>

                        </div>
                    </div>

                </div>
                
            </div>
        </div>

    </div>

</div>




    <script>


                
        function getdatapemesanan(){


            var id=$("#no_pemesanan").val();
            $.ajax({
                type: "POST",
                url: '?aksi=getdatapemesanan&id='+id,
                data: "check",
                success: function(response){
                    
                    var jumlah=$('#jumlah').val();
                    var totalharga=response.harga_jual*jumlah;
                   

                    if(response.nama_obat!='undefined'){

                        var fieldHTML = '<div class="columns" id="input-obat">'
                            +'<div class="column is-4">'+response.id+'</div>'
                            +'<div class="column is-4">'+response.nama_obat+'</div>'
                            +'<div class="column is-4">'+response.jumlah+'</div>'
                        +'</div>'; //New input field html

                        document.getElementById("kirim").disabled = false;


                    }else{

                        var fieldHTML = '<div class="columns" id="input-obat">'
                            +'<div class="column is-4">Tidak Ada data ditemukan</div>'
                            
                        +'</div>'; //New input field html

                        document.getElementById("kirim").disabled = true;


                    }
                    
                    
                    $('#data-obat').html(fieldHTML); //Add field html

                    // var totalhargapasien=document.getElementById("totalharga").innerHTML;

                    // totalhargapasien=parseInt(totalhargapasien)+totalharga;
                    
                    // $("#totalharga").html(totalhargapasien);
                    // $("#totalbayar").val(totalhargapasien);

                     
                }
            });

        }
    </script>