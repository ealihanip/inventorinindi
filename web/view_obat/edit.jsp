

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>
            
        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p>Tambah Obat</p>

        </div>

        <div class="card-content">

            <form method="post" action="obat?aksi=update&id=${obat.id}">
                <div class="field">
                    <label class="label">Nama Obat</label>
                    <div class="control">
                        <input class="input" name="nama" type="text" placeholder="Nama Obat" value="${obat.nama}">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Satuan</label>
                    <div class="control">
                        <input class="input" name="satuan" type="text" placeholder="Satuan" value="${obat.satuan}">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Harga Beli</label>
                    <div class="control">
                        <input class="input" name="harga_beli" type="text" placeholder="harga_beli" value="${obat.harga_beli}">
                    </div>
                </div>

                <div class="field">
                    <label class="label">HPP</label>
                    <div class="control">
                        <input class="input" name="hpp" type="text" placeholder="hpp" value="${obat.hpp}">
                    </div>
                </div>

                <div class="field">
                    <label class="label">Harga Jual</label>
                    <div class="control">
                        <input class="input" name="harga_jual" type="text" placeholder="harga_jual" value="${obat.harga_jual}">
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <input type="submit" class="button is-link" value="submit">
                    </div>
                    <div class="control">
                        <a href="obat" class="button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>