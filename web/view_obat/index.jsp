<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>OBAT</p>

        </div>

        <div class="card-content">

            <a href="obat?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>

                        <td>
                            Nama
                        </td>

                        <td>
                            Satuan
                        </td>

                        <td>
                            Harga Beli
                        </td>

                        <td>
                            HPP
                        </td>

                        <td>
                            Harga Jual
                        </td>

                        <td>
                            aksi
                        </td>
                    </tr>
                </thead>

                </tbody>
                <c:forEach items="${obat}" var="obat">
                    <tr>
                        <td>
                            <c:out value="${obat.id}" />
                        </td>
                        <td>
                            <c:out value="${obat.nama}" />
                        </td>
                        <td>
                            <c:out value="${obat.satuan}" />
                        </td>
                        <td>
                            <c:out value="${obat.harga_beli}" />
                        </td>

                        <td>
                            <c:out value="${obat.hpp}" />
                        </td>

                        <td>
                            <c:out value="${obat.harga_jual}" />
                        </td>

                        <td>
                            <a class='button has-background-warning' href="obat?aksi=edit&id=<c:out value="${obat.id}" />">Edit</a>
                            <a class='button has-background-danger' href="obat?aksi=delete&id=<c:out value="${obat.id}" />">hapus</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>