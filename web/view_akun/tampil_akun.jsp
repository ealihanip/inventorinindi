<%@page import="java.sql.*" %>

<%
    //--koneksi database--
    Connection koneksi = null;
    Statement stmt = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/inventori",
                    "root", "");
    stmt = koneksi.createStatement();
    rs = stmt.executeQuery("SELECT * FROM master_akun");

%>
<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Detail Jurnal</p>

        </div>

        <div class="card-content">

            <a href="index.jsp?halaman=add_akun">Tambah Akun</a>
            <br>
            <br>
            <table border="1" class="table table-bordered jdtable">
                <thead>
                    <tr>
                        <th>Kode*</th>
                        <th>Nama Akun</th>
                        <th>Jenis Akun</th>
                        <th>Saldo Normal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <%while (rs.next()) {

                    out.println("<tr>"
                            + "<td>" + rs.getString("kode_akun") + "</td>"
                            + "<td>" + rs.getString("nama_akun") + "</td>"
                            + "<td>" + rs.getString("jenis_akun") + "</td>"
                            + "<td>" + rs.getString("saldo_normal") + "</td>"
                            + "<td><a href=index.jsp?halaman=edit_akun&kode="
                            + rs.getString("kode_akun") + ">Edit</a> | "
                            + "<a href=akunServlet?aksi=Delete&kode="
                            + rs.getString("kode_akun") + ">Hapus</a></td>"
                            + "</tr>");
                }

            %>
                </tbody>
            </table>

        </div>

    </div>

</div>