<%@page import="java.sql.*, model.Akun" %>
<%
    Akun akun = new Akun();
    //--koneksi database--
    Connection koneksi = null;
    Statement stmt = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/inventori",
                    "root", "");
    stmt = koneksi.createStatement();
    String kode = request.getParameter("kode");
    if (kode != null) {
        rs = stmt.executeQuery("SELECT * FROM master_akun"
                + " WHERE kode_akun = '" + kode + "'");
        if (rs.next()) {
            akun.setKode_akun(rs.getString("kode_akun"));
            akun.setNama_akun(rs.getString("nama_akun"));
            akun.setJenis_akun(rs.getString("jenis_akun"));
            akun.setSaldo_normal(rs.getString("saldo_normal"));
        }
    }


%>

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Detail Jurnal</p>

        </div>

        <div class="card-content">

            <form action="akunServlet" method="POST">
                <table border="0" cellpadding="4">
                    <tbody>
                        <tr>
                            <td valign="top">Jenis Akun</td>
                            <td><select name="jenis">
                                    <!--Jenis Akun disesuaikan sendiri-->
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Kas/Bank")) { %>
                                    <option value="Kas/Bank" selected>Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Aktiva Lancar")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar" selected>Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Aktiva Tetap")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap" selected>Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Kewajiban")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban" selected>Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Modal")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal" selected>Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Pendapatan")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan" selected>Pendapatan</option>
                                    <option value="Beban">Beban</option>
                                    <% }%>
                                    <% if (akun.getJenis_akun()
                                                                                    .equalsIgnoreCase("Beban")) { %>
                                    <option value="Kas/Bank">Kas/Bank</option>
                                    <option value="Aktiva Lancar">Aktiva Lancar</option>
                                    <option value="Aktiva Tetap">Aktiva Tetap</option>
                                    <option value="Kewajiban">Kewajiban</option>
                                    <option value="Modal">Modal</option>
                                    <option value="Pendapatan">Pendapatan</option>
                                    <option value="Beban" selected>Beban</option>
                                    <% }%>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td>Kode*</td>
                            <td><input type="text" name="kode" size="5" value="<%= akun.getKode_akun()%>" readonly /></td>
                        </tr>
                        <tr>
                            <td>Nama Akun</td>
                            <td><input type="text" name="nama" size="20" value="<%=akun.getNama_akun()%>" /></td>
                        </tr>
                        <tr>
                            <td>Saldo Normal</td>
                            <td>
                                <select name="saldo">
                                    <%
                                                        if (akun.getSaldo_normal()
                                                                .equalsIgnoreCase("Debet")) { %>
                                    <option value="Debet" selected>Debet</option>
                                    <option value="Kredit">Kredit</option>
                                    <% } else { %>
                                    <option value="Debet">Debet</option>
                                    <option value="Kredit" selected>Kredit</option>
                                    <% }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Update" name="aksi" /><a href="index.jsp?halaman=data_akun"/>Cencel</a></td>
                                
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>

    </div>

</div>