

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>obatmasuk</p>

        </div>

        <div class="card-content">

            <a href="obatmasuk?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>

                        <td>
                            Nama
                        </td>
                        
                        <td>
                            Jumlah
                        </td>
                        
                        <td>
                            Tanggal
                        </td>

                        <td>
                            aksi
                        </td>
                    </tr>  
                </thead>

                </tbody>
                <c:forEach items="${obatmasuk}" var="obatmasuk">
                    <tr>
                        <td>
                            <c:out value="${obatmasuk.id}" />
                        </td>
                        <td>
                            <c:out value="${obatmasuk.nama_obat}" />
                        </td>
                        <td>
                            <c:out value="${obatmasuk.jumlah}" />
                        </td>
                        <td>
                            <c:out value="${obatmasuk.tanggal}" />
                        </td>
                        
                        <td>
                            <a class='button has-background-warning' href="obatmasuk?aksi=edit&id=<c:out value="${obatmasuk.id}"/>">Edit</a>
                            <a class='button has-background-danger' href="obatmasuk?aksi=delete&id=<c:out value="${obatmasuk.id}"/>">hapus</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>