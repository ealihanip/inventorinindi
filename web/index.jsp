<%
    
    if (session.getAttribute("role") == null) {

        response.sendRedirect("login");
    }
    
%>


<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="includes/head.jsp" %>

    </head>
    <body class="has-background-light">
        <%@include file="includes/nav.jsp" %>
        <section class="section">
            <div class="columns is-multiline">
                <div class="column is-3">
                    <div class="card">
                        <div class="card-content">
                            <%@include file="includes/sidebar.jsp" %>
                        </div>
                    </div>

                </div>



                <%
                    if (session.getAttribute("role") != null) {
                        if (session.getAttribute("role").equals("Logistik")) {%>
                <c:choose>


                    <c:when test="${param.halaman=='obat'}">
                        <%@include file="view_obat/index.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='create_obat'}">
                        <%@include file="view_obat/create.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_obat'}">
                        <%@include file="view_obat/edit.jsp" %>
                    </c:when>



                    <c:when test="${param.halaman=='stok'}">
                        <%@include file="view_stok/index.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='obatmasuk'}">
                        <%@include file="view_obatmasuk/index.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='create_obatmasuk'}">
                        <%@include file="view_obatmasuk/create.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='edit_obatmasuk'}">
                        <%@include file="view_obatmasuk/edit.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='obatkeluar'}">
                        <%@include file="view_obatkeluar/index.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='create_obatkeluar'}">
                        <%@include file="view_obatkeluar/create.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='edit_obatkeluar'}">
                        <%@include file="view_obatkeluar/edit.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='pemesanan'}">
                        <%@include file="view_pemesanan/index.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='create_pemesanan'}">
                        <%@include file="view_pemesanan/create.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='edit_pemesanan'}">
                        <%@include file="view_pemesanan/edit.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='cetak_pemesanan'}">
                        <%@include file="view_pemesanan/cetak_pemesanan.jsp" %>
                    </c:when>

                    <c:otherwise>
                        <%@include file="home/index.jsp" %>
                    </c:otherwise>
                </c:choose>     

                <%        
                        }
                    }
                %>


                <%
            if (session.getAttribute("role") != null) {
                if (session.getAttribute("role").equals("Keuangan")) {%>
                <c:choose>

                    <c:when test="${param.halaman=='pembelian'}">
                        <%@include file="view_pembelian/index.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='create_pembelian'}">
                        <%@include file="view_pembelian/create.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='edit_pembelian'}">
                        <%@include file="view_pembelian/edit.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='data_akun'}">
                        <%@include file="view_akun/tampil_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='add_akun'}">
                        <%@include file="view_akun/add_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_akun'}">
                        <%@include file="view_akun/edit_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_akun'}">
                        <%@include file="view_akun/info_akun.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='informasi_jurnal'}">
                        <%@include file="view_jurnal/info_jurnal.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_jurnal_detail'}">
                        <%@include file="view_jurnal/info_jurnal_detail.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='jurnal_umum'}">
                        <%@include file="view_jurnal/jurnal_umum.jsp" %>
                    </c:when>

                    <c:otherwise>
                        <%@include file="home/index.jsp" %>
                    </c:otherwise>
                </c:choose>
                <%       }
                    }
                %>

                <%
        if (session.getAttribute("role") != null) {
            if (session.getAttribute("role").equals("Admin")) {%>
                <c:choose>


                    <c:when test="${param.halaman=='user'}">
                        <%@include file="view_user/index.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='create_user'}">
                        <%@include file="view_user/create.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_user'}">
                        <%@include file="view_user/edit.jsp" %>
                    </c:when>


                    <c:otherwise>
                        <%@include file="home/index.jsp" %>
                    </c:otherwise>
                </c:choose>
                <%        }
                    }
                %>

                <%
                if (session.getAttribute("role") != null) {
                    if (session.getAttribute("role").equals("Pimpinan")) {%>
                        <c:choose>
        
        
                            <c:when test="${param.halaman=='laporan_pembelian'}">
                                <%@include file="view_report/laporan_pembelian.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_stok_harian'}">
                                <%@include file="view_report/laporan_stok_harian.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_stok_bulanan'}">
                                <%@include file="view_report/laporan_stok_bulanan.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_stok_tahunan'}">
                                <%@include file="view_report/laporan_stok_tahunan.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_stok_bulanan'}">
                                <%@include file="view_report/laporan_stok_bulanan.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_jurnal_harian'}">
                                <%@include file="view_report/laporan_jurnal_harian.jsp" %>
                            </c:when>
                            <c:when test="${param.halaman=='laporan_jurnal_bulanan'}">
                                <%@include file="view_report/laporan_jurnal_bulanan.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='laporan_jurnal_tahunan'}">
                                <%@include file="view_report/laporan_jurnal_tahunan.jsp" %>
                            </c:when>


                            
        
                            
        
        
                            <c:otherwise>
                                <%@include file="home/index.jsp" %>
                            </c:otherwise>
                        </c:choose>
                        <%        }
                            }
                        %>


                <%
                if (session.getAttribute("role") != null) {
                    if (session.getAttribute("role").equals("Kasir")) {%>
                        <c:choose>
        
        
                            <c:when test="${param.halaman=='penjualan'}">
                                <%@include file="view_penjualan/index.jsp" %>
                            </c:when>

                            <c:when test="${param.halaman=='create_penjualan'}">
                                <%@include file="view_penjualan/create.jsp" %>
                            </c:when>


                            <c:when test="${param.halaman=='stok'}">
                                <%@include file="view_stok/index.jsp" %>
                            </c:when>


                            <c:when test="${param.halaman=='detail_penjualan'}">
                                <%@include file="view_penjualan/detail_penjualan.jsp" %>
                            </c:when>

        
                            
        
        
                            <c:otherwise>
                                <%@include file="home/index.jsp" %>
                            </c:otherwise>
                        </c:choose>
                        <%        }
                            }
                        %>

            </div>


        </section>
        <%@include file="includes/footer.jsp" %>
        <%@include file="includes/script.jsp" %>
        <script src="config/setting.js" ></script>
    </body>
</html>