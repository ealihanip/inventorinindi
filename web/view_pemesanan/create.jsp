<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>


    <div class="card">

        <div class="card-header-title">

            <p>Tambah Pemesanan</p>

        </div>

        <div class="card-content">



            <form method="post" action="pemesanan?aksi=store">
                

                <div class="field">
                    <label class="label">Nama Obat</label>
                    <div class="control">
                        <div class="select is-primary">
                            <select name="obat_id">

                                <c:forEach items="${obat}" var="obat">

                                    <option value="<c:out value="${obat.id}" />" >
                                    <c:out value="${obat.nama}" />
                                    </option>


                                </c:forEach>


                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Jumlah</label>
                    <div class="control">
                        <input class="input" name="jumlah" type="text" placeholder="Jumlah">
                    </div>
                </div>
                

                <div class="field">
                    <label class="label">Tanggal</label>
                    <div class="control">
                        <input class="input tanggal" name="tanggal" type="text" placeholder="Tanggal">
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <input type="submit" class="button is-link" value="submit">
                    </div>
                    <div class="control">
                        <a href="pemesanan" class="button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>