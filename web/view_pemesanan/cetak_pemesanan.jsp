<%
    
    if (session.getAttribute("role") == null) {

        response.sendRedirect("login");
    }
    
%>


<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


</head>

<body class="">

    <div id='body'>
        
        
        <div class="container">

            <p class="h1">Pemesanan Obat</p>
            <hr>
            <div class="row">

                <div class="col-md-2">

                    No Pemesanan

                </div>

                <div class="col-md-10">
                    : ${pemesanan.id}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Tanggal

                </div>

                <div class="col-md-10">
                    : ${pemesanan.tanggal}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Kode Obat

                </div>

                <div class="col-md-10">
                    : ${pemesanan.obat_id}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Nama Obat

                </div>

                <div class="col-md-10">
                    : ${pemesanan.nama_obat}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Jumlah

                </div>

                <div class="col-md-10">
                    : ${pemesanan.jumlah}
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">

                    Status

                </div>

                <div class="col-md-10">
                    : ${pemesanan.status}
                </div>

            </div>

        </div>




    </div>


    <script>

        window.print();
        
    </script>
</body>

</html>