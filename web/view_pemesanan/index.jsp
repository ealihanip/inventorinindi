<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Pemesanan</p>

        </div>

        <div class="card-content">

            <a href="pemesanan?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            No Pemesanan
                        </td>

                        <td>
                            Nama Obat
                        </td>


                        <td>
                            Jumlah
                        </td>




                        <td>
                            Tanggal
                        </td>

                        <td>
                            Status
                        </td>

                        <td>
                            aksi
                        </td>
                    </tr>
                </thead>

                </tbody>
                <c:forEach items="${pemesanan}" var="pemesanan">
                    <tr>
                        <td>
                            <c:out value="${pemesanan.id}" />
                        </td>

                        <td>
                            <c:out value="${pemesanan.nama_obat}" />
                        </td>

                        <td>
                            <c:out value="${pemesanan.jumlah}" />
                        </td>

                        <td>
                            <c:out value="${pemesanan.tanggal}" />
                        </td>

                        <td>
                            <c:out value="${pemesanan.status}" />
                        </td>

                        <td>


                                <a class='button has-background-primary' target='_blank' href="pemesanan?aksi=cetak&id=<c:out value="${pemesanan.id}" />">Cetak</a>
                            <c:choose>
                                <c:when test="${pemesanan.status=='Dalam Proses'}">
                                        <a class='button has-background-danger' href="pemesanan?aksi=delete&id=<c:out value="${pemesanan.id}" />">Batal</a>
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                            </c:choose>
                           


                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>