<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Stok</p>

        </div>

        <div class="card-content">

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>

                        <td>
                            Nama Barang
                        </td>

                        <td>
                            Jumlah
                        </td>
                        
                        <td>
                            Update Pada
                        </td>
                    </tr>
                </thead>

                </tbody>
                <c:forEach items="${Stok}" var="Stok">
                    <tr>
                        <td>
                    <c:out value="${Stok.id}" />
                    </td>
                    <td>
                    <c:out value="${Stok.nama_obat}" />
                    </td>
                    <td>
                    <c:out value="${Stok.jumlah}" />
                    </td>
                    <td>
                    <c:out value="${Stok.update_at}" />
                    </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>