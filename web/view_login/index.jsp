<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
    <%@include file="../includes/head.jsp" %>

</head>

<body class="">
    <%@include file="../includes/nav.jsp" %>
    <section class="section">
        <div class="columns is-centered">

            <div class="column is-4">
                <center>
                    <p class="is-size-1">Login</p>
                </center>
            </div>



        </div>

        <div class="columns is-centered">

            <div class="column is-4 has-background-light">
                <c:choose>
                    <c:when test="${status=='error'}">
                        <div class="notification is-danger">
                            <button class="delete"></button>

                            ${message}

                        </div>
                    </c:when>
                    <c:otherwise>

                    </c:otherwise>
                </c:choose>
            </div>



        </div>
        <div class="columns is-centered">

            <div class="column is-4 has-background-light">

                <form method="post" action="login?aksi=proses_login">
                    <div class="field">
                        <label class="label">Username</label>
                        <div class="control">
                            <input class="input" type="text" name="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Password</label>
                        <div class="control">
                            <input class="input" type="password" name="password" placeholder="Password">
                        </div>
                    </div>

                    <div class="field">

                        <div class="control">
                            <input type="submit" class="button is-primary" value="Login">
                        </div>
                    </div>


            </div>

        </div>
    </section>

    <%@include file="../includes/script.jsp" %>
    <script src="config/setting.js"></script>
</body>

</html>