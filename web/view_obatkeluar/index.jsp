

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>obatkeluar</p>

        </div>

        <div class="card-content">

            <a href="obatkeluar?aksi=create" class="button has-background-primary">Tambah Data </a>

            <table class="display jdtable">
                <thead>
                    <tr>
                        <td>
                            Id
                        </td>

                        <td>
                            Nama
                        </td>
                        
                        <td>
                            Jumlah
                        </td>
                        
                        <td>
                            Tanggal
                        </td>

                        <td>
                            aksi
                        </td>
                    </tr>  
                </thead>

                </tbody>
                <c:forEach items="${obatkeluar}" var="obatkeluar">
                    <tr>
                        <td>
                            <c:out value="${obatkeluar.id}" />
                        </td>
                        <td>
                            <c:out value="${obatkeluar.nama_obat}" />
                        </td>
                        <td>
                            <c:out value="${obatkeluar.jumlah}" />
                        </td>
                        <td>
                            <c:out value="${obatkeluar.tanggal}" />
                        </td>
                        
                        <td>
                            <a class='button has-background-warning' href="obatkeluar?aksi=edit&id=<c:out value="${obatkeluar.id}"/>">Edit</a>
                            <a class='button has-background-danger' href="obatkeluar?aksi=delete&id=<c:out value="${obatkeluar.id}"/>">hapus</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </div>

</div>