<%-- 
    Document   : tampil_user
    Created on : Apr 20, 2018, 9:30:20 PM
    Author     : Nindi Musliyanti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Detail Jurnal</p>

        </div>

        <div class="card-content">

            <a class="btn btn-primary" href="index.jsp?halaman=informasi_jurnal">Kembali<a />
                <hr>
                <table class="table table-bordered jdtable" id="tabel">
                    <thead>
                        <tr>
                            <th> No Referensir </th>
                            <th> Kode Akun </th>
                            <th> Debet </th>
                            <th> Kredit </th>
                        </tr>
                    </thead>

                    <tbody>

                        <%
                            Connection koneksi = null;
                            Statement stmt = null;
                            ResultSet rs = null;
                            String id = request.getParameter("id");
                            Class.forName("com.mysql.jdbc.Driver");
                            koneksi = DriverManager.getConnection("jdbc:mysql://localhost/inventori", "root", "");
                            stmt = koneksi.createStatement();
                            rs = stmt.executeQuery("SELECT * FROM jurnal_detail WHERE no_referensi = '" + id + "'");
            
                            int isi = rs.getRow();
                            //  out.println(isi);
                            if (isi > 0) {
                                out.println("<tr>"
                                        + "<td colspan=5>"
                                        + " Data Kosong -"
                                        + "</td>"
                                        + "</tr>");
                            } else {
                                while (rs.next()) {
                                    out.println("<tr>"
                                            + "<td>" + rs.getString(1) + "</td>"
                                            + "<td>" + rs.getString(2) + "</td>"
                                            + "<td>" + rs.getString(3) + "</td>"
                                            + "<td>" + rs.getString(4) + "</td>"
                                            + "</tr>");
                                }
                            }
                        %>

                    </tbody>
                </table>

        </div>

    </div>

</div>