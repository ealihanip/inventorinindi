<%-- 
    Document   : rekap_ju_perbulan
    Created on : Oct 23, 2018, 10:23:29 PM
    Author     : ASUS
--%>

<%@page import="java.io.*, java.util.*, java.sql.*"%> 
<%@page import="net.sf.jasperreports.engine.*"%> 
<%@page import="net.sf.jasperreports.view.JasperViewer.*" %> 
<%@page import="javax.servlet.ServletResponse" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html> 
<html>

<head>
    
<title>Laporan Pembelian</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<title>JSP Page</title> </head> 
<body> 
<% Connection conn = null; 
//--koneksikeMySQL database: sia1 (sesuaikandengandatabase masing-masing) 
String url="jdbc:mysql://localhost:3306/inventori"; 
String username="root"; 
String password="";            
String tanggal=request.getParameter("tanggal") ; 

Class.forName("com.mysql.jdbc.Driver"); 
conn = DriverManager.getConnection(url, username, password);
File reportFile= new File(application.getRealPath("report/stok_harian.jasper"));
Map parameter = new HashMap(); 
parameter.put("tanggal", tanggal); 
byte[] bytes = JasperRunManager 
.runReportToPdf(reportFile.getPath(), parameter, conn);
response.setContentType("application/pdf"); 
response.setContentLength(bytes.length);
ServletOutputStream outStream= response.getOutputStream(); 
outStream.write(bytes, 0, bytes.length); 
outStream.flush(); 
outStream.close(); 
%>
</body> 

</html>
