

<div class="column is-9">

    <c:choose>
        <c:when test="${status=='success'}">
            <div class="notification is-primary">
                <button class="delete"></button>

                Berhasil

            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
    <div class="card">

        <div class="card-header-title">

            <p>Laporan Jurnal Bulanan</p>

        </div>

        <div class="card-content">

            <form action="view_report/show_laporan_jurnal_bulanan.jsp" target="_blank"> 
                Masukkan Bulan: 
                <select name="bln" id="bln"> 
                <option value="01">Januari</option>   
                <option value="02">Februari</option>   
                <option value="03">Maret</option>   
                <option value="04">April</option>   
                <option value="05">Mei</option>   
                <option value="06">Juni</option>   
                <option value="07">Juli</option>   
                <option value="08">Agustus</option> 
                <option value="09">September</option>   
                <option value="10">Oktober</option>   
                <option value="11">November</option>   
                <option value="12">Desember</option>   
                </select><br>
                Masukkan Tahun: 
                <select name="thn" id="thn"> 
                <option value="2010">2010</option>   
                <option value="2011">2011</option>   
                <option value="2012">2012</option>   
                <option value="2013">2013</option>   
                <option value="2014">2014</option>   
                <option value="2015">2015</option>   
                <option value="2016">2016</option>   
                <option value="2017">2017</option>  
                <option value="2018">2018</option>   
                <option value="2019">2019</option>   
                <option value="2020">2020</option>      
                </select> 
                <br> 
                <input type="submit" value="Cetak"> 
                </form> 
            

        </div>

    </div>

</div>