/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pemesanan;
import model.Obat;
import java.util.Date;

/**
 *
 * @author kanep
 */
public class PemesananController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventori", "root", "");

            String forward = "";
            String aksi = "";
            Pemesanan pemesanan = new Pemesanan();
            Obat obat = new Obat();
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            RequestDispatcher view = null;
            Statement statement = null;
            List<Obat> obats = new ArrayList<>();
            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());

            //jika tidak ada request di anggap index
            if (request.getParameter("aksi") != null) {

                aksi = request.getParameter("aksi");

            }

            //handle page
            switch (aksi) {


                case "getdatapemesanan":

                    
                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from pemesanan join obat on obat.id=pemesanan.obat_id where pemesanan.id=?");
                    preparedStatement.setInt(1, pemesanan.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        pemesanan = new Pemesanan();
                        pemesanan.setId(rs.getInt("id"));

                        pemesanan.setObat_id(rs.getInt("obat_id"));
                        pemesanan.setNama_obat(rs.getString("nama"));
                        pemesanan.setJumlah(rs.getInt("jumlah"));
                        pemesanan.setStatus(rs.getString("status"));

                        pemesanan.setTanggal(rs.getString("tanggal"));

                    }

                    

                    String json = new Gson().toJson(pemesanan); // anyObject = List<Bean>, Map<K, Bean>, Bean, String, etc..
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json);
                break;
                case "create":

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));

                        obats.add(obat);
                    }

                    request.setAttribute("obat", obats);

                    forward = "/index.jsp?halaman=create_pemesanan";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;
                case "edit":

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));

                        obats.add(obat);
                    }

                    request.setAttribute("obat", obats);

                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from pemesanan where id=?");
                    preparedStatement.setInt(1, pemesanan.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        pemesanan = new Pemesanan();
                        pemesanan.setId(rs.getInt("id"));

                        pemesanan.setObat_id(rs.getInt("obat_id"));
                        pemesanan.setJumlah(rs.getInt("jumlah"));

                        pemesanan.setTanggal(rs.getString("tanggal"));

                    }
                    forward = "/index.jsp?halaman=edit_pemesanan";
                    request.setAttribute("pemesanan", pemesanan);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);

                    break;
                case "store":

                    try {
                        pemesanan = new Pemesanan();

                        pemesanan.setObat_id(Integer.parseInt(request.getParameter("obat_id")));
                        pemesanan.setJumlah(Integer.parseInt(request.getParameter("jumlah")));

                        pemesanan.setTanggal(request.getParameter("tanggal"));
                        pemesanan.setCreate_at(date);

                        preparedStatement = koneksi.prepareStatement("insert into pemesanan(obat_id,jumlah,tanggal,create_at) values (?,?,?,?)");

                        preparedStatement.setInt(1, pemesanan.getObat_id());
                        preparedStatement.setInt(2, pemesanan.getJumlah());

                        preparedStatement.setString(3, pemesanan.getTanggal());
                        preparedStatement.setString(4, pemesanan.getCreate_at());
                        preparedStatement.executeUpdate();

                        forward = "pemesanan?aksi=create&status=success";
                        response.sendRedirect(forward);
                    } catch (NumberFormatException | SQLException e) {

                        out.println(e);
                    }
                    break;
                case "update":

                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    pemesanan.setObat_id(Integer.parseInt(request.getParameter("obat_id")));
                    pemesanan.setJumlah(Integer.parseInt(request.getParameter("jumlah")));

                    pemesanan.setTanggal(request.getParameter("tanggal"));
                    pemesanan.setUpdate_at(date);

                    preparedStatement = koneksi.prepareStatement("update pemesanan set "
                            + "obat_id=?,"
                            + "jumlah=?,"
                            + "tanggal=?,"
                            + "update_at=? "
                            + "where id=?");

                    preparedStatement.setInt(1, pemesanan.getObat_id());
                    preparedStatement.setInt(2, pemesanan.getJumlah());
                    preparedStatement.setString(3, pemesanan.getTanggal());
                    preparedStatement.setString(4, pemesanan.getUpdate_at());
                    preparedStatement.setInt(5, pemesanan.getId());
                    preparedStatement.executeUpdate();

                    forward = "pemesanan?aksi=edit&id=" + pemesanan.getId() + "&status=success";
                    response.sendRedirect(forward);

                    out.println(pemesanan.toString());

                    break;

                case "cetak":
                    
                    

                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from pemesanan join obat on obat.id=pemesanan.obat_id where pemesanan.id=?");
                    preparedStatement.setInt(1, pemesanan.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        pemesanan = new Pemesanan();
                        pemesanan.setId(rs.getInt("id"));

                        pemesanan.setObat_id(rs.getInt("obat_id"));
                        pemesanan.setNama_obat(rs.getString("nama"));
                        pemesanan.setJumlah(rs.getInt("jumlah"));
                        pemesanan.setStatus(rs.getString("status"));

                        pemesanan.setTanggal(rs.getString("tanggal"));

                    }
                    forward = "/view_pemesanan/cetak_pemesanan.jsp";
                    request.setAttribute("pemesanan", pemesanan);

                    

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);

                    break;
                case "delete":

                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi
                            .prepareStatement("delete from pemesanan where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, pemesanan.getId());
                    preparedStatement.executeUpdate();

                    forward = "pemesanan?status=success";

                    response.sendRedirect(forward);
                    break;

                default:

                    try {

                        List<Pemesanan> pemesanans = new ArrayList<>();

                        statement = koneksi.createStatement();

                        rs = statement.executeQuery("select * from pemesanan join obat on obat.id=pemesanan.obat_id");
                        while (rs.next()) {
                            pemesanan = new Pemesanan();
                            pemesanan.setId(rs.getInt("id"));
                            pemesanan.setObat_id(rs.getInt("obat_id"));
                            pemesanan.setNama_obat(rs.getString("nama"));
                            pemesanan.setJumlah(rs.getInt("jumlah"));
                            pemesanan.setTanggal(rs.getString("tanggal"));
                            pemesanan.setStatus(rs.getString("status"));

                            pemesanans.add(pemesanan);
                        }

                        forward = "/index.jsp?halaman=pemesanan";
                        request.setAttribute("pemesanan", pemesanans);

                        if (request.getParameter("status") != null) {

                            String status = request.getParameter("status");
                            if (status.equals("success")) {

                                request.setAttribute("status", "success");
                                request.setAttribute("message", "Berhasil");

                            }
                        }

                        view = request.getRequestDispatcher(forward);
                        view.forward(request, response);
                    } catch (NumberFormatException | SQLException e) {

                        out.println(e);
                    }

                    break;

            }
            //end handle page

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PemesananController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PemesananController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void elseif(boolean equals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
