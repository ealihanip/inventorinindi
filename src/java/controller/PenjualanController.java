/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Penjualan;
import model.DetailPenjualan;
import model.Obat;
import model.Stok;
import java.util.Date;

/**
 *
 * @author kanep
 */
public class PenjualanController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventori", "root", "");

            String forward = "";
            String aksi = "";
            Penjualan penjualan = new Penjualan();
            DetailPenjualan detailpenjualan = new DetailPenjualan();
            Obat obat = new Obat();
            Stok stok = new Stok();
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            RequestDispatcher view = null;
            Statement statement = null;
            List<Obat> obats = new ArrayList<>();
            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());

            //jika tidak ada request di anggap index
            if (request.getParameter("aksi") != null) {

                aksi = request.getParameter("aksi");

            }

            //handle page
            switch (aksi) {

                case "getdataobat":

                    
                    obat = new Obat();
                    obat.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from obat where id=?");
                    preparedStatement.setInt(1, obat.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));
                        obat.setSatuan(rs.getString("satuan"));
                        obat.setHarga_beli(rs.getString("harga_beli"));
                        obat.setHpp(rs.getString("hpp"));
                        obat.setHarga_jual(rs.getString("harga_jual"));

                    }

                    

                    String json = new Gson().toJson(obat); // anyObject = List<Bean>, Map<K, Bean>, Bean, String, etc..
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json);
                break;

                case "store":
                    

                    //menangkap list obat
                    String[] obat_id = request.getParameterValues("id[]");
                    String[] jumlah = request.getParameterValues("jumlah[]");

                    
                    penjualan = new Penjualan();
                    penjualan.setNo_register(request.getParameter("no_register"));
                    penjualan.setTanggal(request.getParameter("tanggal"));
                    penjualan.setNama_pasien(request.getParameter("nama_pasien"));
                    penjualan.setAlamat(request.getParameter("alamat"));
                    penjualan.setPoliklinik(request.getParameter("poliklinik"));
                    penjualan.setDokter(request.getParameter("dokter"));
                    penjualan.setBayar(request.getParameter("totalbayar"));
                    penjualan.setCreate_at(date);
                    

                   

                    
                    
                    preparedStatement = koneksi.prepareStatement("insert into penjualan"
                    +"(no_register,tanggal,nama_pasien,alamat,poliklinik,dokter,bayar,create_at) "
                    +"values (?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                    preparedStatement.setString(1, penjualan.getNo_register());
                    preparedStatement.setString(2, penjualan.getTanggal());
                    preparedStatement.setString(3, penjualan.getNama_pasien());
                    preparedStatement.setString(4, penjualan.getAlamat());
                    preparedStatement.setString(5, penjualan.getPoliklinik());
                    preparedStatement.setString(6, penjualan.getDokter());
                    preparedStatement.setString(7, penjualan.getBayar());
                    preparedStatement.setString(8, penjualan.getCreate_at());
                    preparedStatement.executeUpdate();
                    
                    //get last id
                    rs = preparedStatement.getGeneratedKeys();
                    int penjualan_id=0;
                    if (rs.next()) {
                        
                        penjualan.setId(rs.getInt(1));
                    } 

                    for (int i = 0; i < obat_id.length; i++) {

                        detailpenjualan = new DetailPenjualan();
                        detailpenjualan.setPenjualan_id(penjualan.getId());
                        detailpenjualan.setObat_id(Integer.parseInt(obat_id[i]));
                        detailpenjualan.setJumlah(Integer.parseInt(jumlah[i]));
                        
                        detailpenjualan.setCreate_at(date);
                        
                        preparedStatement = koneksi.prepareStatement("insert into detail_penjualan"
                        +"(penjualan_id,obat_id,jumlah,create_at) "
                        +"values (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                        preparedStatement.setInt(1, detailpenjualan.getPenjualan_id());
                        preparedStatement.setInt(2, detailpenjualan.getObat_id());
                        preparedStatement.setInt(3, detailpenjualan.getJumlah());
                        preparedStatement.setString(4, detailpenjualan.getCreate_at());
                        preparedStatement.executeUpdate();

                        //get data stok

                        preparedStatement = koneksi.prepareStatement("select * from stok where obat_id=?");
                        preparedStatement.setInt(1, detailpenjualan.getObat_id());
                        rs = preparedStatement.executeQuery();
                        while (rs.next()) {

                            stok = new Stok();
                            stok.setId(rs.getInt("id"));
                            stok.setJumlah(rs.getInt("jumlah"));
                        }

                        //update stok
                        int stokbaru = stok.getJumlah();

                        stokbaru=stokbaru-detailpenjualan.getJumlah();

                     
                        //update ke stok
                        stok.setJumlah(stokbaru);
                        stok.setUpdate_at(date);

                        preparedStatement = koneksi.prepareStatement("update stok set jumlah=?,update_at=? where id=?");
                        // Parameters start with 1
                        preparedStatement.setInt(1, stok.getJumlah());
                        preparedStatement.setString(2, stok.getUpdate_at());
                        preparedStatement.setInt(3, stok.getId());
                        preparedStatement.executeUpdate();
                        //end updatestok

                    }

                    forward = "penjualan?aksi=create&status=success";
                    response.sendRedirect(forward);



                    
                    
                break;

                case "create":

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));
                        obat.setSatuan(rs.getString("satuan"));
                        obat.setHarga_jual(rs.getString("harga_jual"));

                        obats.add(obat);
                    }

                    request.setAttribute("obat", obats);

                    forward = "/index.jsp?halaman=create_penjualan";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                break;
                case "detailpenjualan":


                penjualan = new Penjualan();
                penjualan.setId(Integer.parseInt(request.getParameter("id")));
                
                
                List<DetailPenjualan> detailpenjualans = new ArrayList<>();

                
                preparedStatement = koneksi.prepareStatement("SELECT * FROM detail_penjualan "
                +"JOIN penjualan ON penjualan.id=detail_penjualan.penjualan_id "
                +"JOIN obat ON obat.id=detail_penjualan.obat_id "
                +"where penjualan.id=?");
                preparedStatement.setInt(1, penjualan.getId());

                rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    detailpenjualan = new DetailPenjualan();
                    detailpenjualan.setId(rs.getInt("id"));
                    detailpenjualan.setObat_id(rs.getInt("obat_id"));
                    detailpenjualan.setNama_obat(rs.getString("nama"));
                    detailpenjualan.setJumlah(rs.getInt("jumlah"));
                    
                    detailpenjualans.add(detailpenjualan);
                }

                forward = "/index.jsp?halaman=detail_penjualan";
                request.setAttribute("detailpenjualan", detailpenjualans);

                if (request.getParameter("status") != null) {

                    String status = request.getParameter("status");
                    if (status.equals("success")) {

                        request.setAttribute("status", "success");
                        request.setAttribute("message", "Berhasil");

                    }
                }
                view = request.getRequestDispatcher(forward);
                view.forward(request, response);
                break;
                default:

                    List<Penjualan> penjualans = new ArrayList<>();

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from penjualan");
                    while (rs.next()) {
                        penjualan = new Penjualan();
                        penjualan.setId(rs.getInt("id"));
                        penjualan.setNo_register(rs.getString("no_register"));
                        penjualan.setTanggal(rs.getString("tanggal"));
                        penjualan.setNama_pasien(rs.getString("nama_pasien"));
                        penjualan.setAlamat(rs.getString("alamat"));
                        penjualan.setPoliklinik(rs.getString("poliklinik"));
                        penjualan.setDokter(rs.getString("dokter"));
                        penjualan.setBayar(rs.getString("bayar"));
                        

                        penjualans.add(penjualan);
                    }

                    forward = "/index.jsp?halaman=penjualan";
                    request.setAttribute("penjualan", penjualans);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }
                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;

            }
            //end handle page

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PenjualanController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PenjualanController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void elseif(boolean equals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
