/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pembelian;
import model.Obat;
import java.util.Date;
import model.Pemesanan;

/**
 *
 * @author kanep
 */
public class PembelianController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventori", "root", "");

            String forward = "";
            String aksi = "";
            Pembelian pembelian = new Pembelian();
            Pemesanan pemesanan = new Pemesanan();
            Obat obat = new Obat();
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            RequestDispatcher view = null;
            Statement statement = null;
            List<Obat> obats = new ArrayList<>();
            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());

            //jika tidak ada request di anggap index
            if (request.getParameter("aksi") != null) {

                aksi = request.getParameter("aksi");

            }

            //handle page
            switch (aksi) {
                
                case "cetak":

                pembelian = new Pembelian();
                pembelian.setId(Integer.parseInt(request.getParameter("id")));


               


                preparedStatement = koneksi.prepareStatement("select * from pembelian join pemesanan "
                + "on pemesanan.id=pembelian.pemesanan_id join obat "
                + "on obat.id=pemesanan.obat_id where pembelian.id=?");
                preparedStatement.setInt(1, pembelian.getId());
                rs = preparedStatement.executeQuery();

                if (rs.next()) {

                        pembelian = new Pembelian();
                        pembelian.setId(rs.getInt("id"));
                        pembelian.setPemesanan_id(rs.getInt("pemesanan_id"));
                        pembelian.setObat_id(rs.getInt("obat_id"));
                        pembelian.setNama_obat(rs.getString("nama"));
                        pembelian.setJumlah(rs.getString("jumlah"));
                        pembelian.setSupplier(rs.getString("supplier"));
                        pembelian.setTotal_tagihan(rs.getString("total_tagihan"));
                        pembelian.setTanggal(rs.getString("tanggal"));
                        pembelian.setKeterangan(rs.getString("keterangan"));
                        pembelian.setJumlah_bayar(rs.getString("jumlah_bayar"));

                }

                

                forward = "/view_pembelian/cetak_pembelian.jsp";
                request.setAttribute("pembelian", pembelian);

                view = request.getRequestDispatcher(forward);
                view.forward(request, response);

                break;
                
                case "getdatapemesanan":

                    
                    pemesanan = new Pemesanan();
                    pemesanan.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from pemesanan join obat on obat.id=pemesanan.obat_id where pemesanan.id=?");
                    preparedStatement.setInt(1, pemesanan.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        pemesanan = new Pemesanan();
                        pemesanan.setId(rs.getInt("id"));

                        pemesanan.setObat_id(rs.getInt("obat_id"));
                        pemesanan.setNama_obat(rs.getString("nama"));
                        pemesanan.setJumlah(rs.getInt("jumlah"));
                        pemesanan.setStatus(rs.getString("status"));

                        pemesanan.setTanggal(rs.getString("tanggal"));

                    }

                    

                    String json = new Gson().toJson(pemesanan); // anyObject = List<Bean>, Map<K, Bean>, Bean, String, etc..
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json);
                break;
                case "create":

                    
                    forward = "/index.jsp?halaman=create_pembelian";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;
                
                case "store":

                    pembelian = new Pembelian();
                    pembelian.setPemesanan_id(Integer.parseInt(request.getParameter("no_pemesanan")));
                    pembelian.setSupplier(request.getParameter("supplier"));
                    pembelian.setTotal_tagihan(request.getParameter("total_tagihan"));
                    pembelian.setTanggal(request.getParameter("tanggal"));
                    pembelian.setKeterangan(request.getParameter("keterangan"));
                    pembelian.setJumlah_bayar(request.getParameter("jumlah_bayar"));
                    
                    
                    
                    try{
                    
                    preparedStatement = koneksi.prepareStatement("insert into pembelian(pemesanan_id,supplier,total_tagihan,jumlah_bayar,keterangan,tanggal,create_at) values (?,?,?,?,?,?,?)");
                    preparedStatement.setInt(1, pembelian.getPemesanan_id());
                    preparedStatement.setString(2, pembelian.getSupplier());
                    preparedStatement.setString(3, pembelian.getTotal_tagihan());
                    preparedStatement.setString(4, pembelian.getJumlah_bayar());
                    preparedStatement.setString(5, pembelian.getKeterangan());
                    preparedStatement.setString(6, pembelian.getTanggal());

                    preparedStatement.setString(7, pembelian.getCreate_at());
                    preparedStatement.executeUpdate();


                    pemesanan = new Pemesanan();
                    pemesanan.setId(pembelian.getPemesanan_id());
                    pemesanan.setStatus("Dibayar");
                    
                    preparedStatement = koneksi
                            .prepareStatement("update pemesanan set status=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setString(1, pemesanan.getStatus());
                    
                    preparedStatement.setInt(2, pemesanan.getId());
                    preparedStatement.executeUpdate();



                } catch (NumberFormatException | SQLException e) {

                    out.println(e);
                }
                    forward = "pembelian?aksi=create&status=success";
                    response.sendRedirect(forward);

                    break;
                
                
                default:

                    List<Pembelian> pembelians = new ArrayList<>();

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from pembelian join pemesanan "
                            + "on pemesanan.id=pembelian.pemesanan_id join obat "
                            + "on obat.id=pemesanan.obat_id");
                    while (rs.next()) {
                        pembelian = new Pembelian();
                        pembelian.setId(rs.getInt("id"));
                        pembelian.setPemesanan_id(rs.getInt("pemesanan_id"));
                        pembelian.setObat_id(rs.getInt("obat_id"));
                        pembelian.setNama_obat(rs.getString("nama"));
                        pembelian.setJumlah(rs.getString("jumlah"));
                        pembelian.setSupplier(rs.getString("supplier"));
                        pembelian.setTotal_tagihan(rs.getString("total_tagihan"));
                        pembelian.setTanggal(rs.getString("tanggal"));
                        pembelian.setKeterangan(rs.getString("keterangan"));
                        pembelian.setJumlah_bayar(rs.getString("jumlah_bayar"));

                        pembelians.add(pembelian);
                    }

                    forward = "/index.jsp?halaman=pembelian";
                    request.setAttribute("pembelian", pembelians);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }
                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;

            }
            //end handle page

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PembelianController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PembelianController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void elseif(boolean equals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
