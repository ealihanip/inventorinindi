/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Obat;
import model.Stok;
import java.util.Date;

/**
 *
 * @author kanep
 */
public class ObatController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventori", "root", "");

            String forward = "";
            String aksi = "";
            Obat obat = new Obat();
            Stok stok = new Stok();
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            RequestDispatcher view = null;
            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            

            //jika tidak ada request di anggap index
            if (request.getParameter("aksi") != null) {

                aksi = request.getParameter("aksi");

            }

            //handle page
            switch (aksi) {

                case "create":
                    forward = "/index.jsp?halaman=create_obat";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;
                case "edit":

                    obat = new Obat();
                    obat.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi.prepareStatement("select * from obat where id=?");
                    preparedStatement.setInt(1, obat.getId());
                    rs = preparedStatement.executeQuery();

                    if (rs.next()) {

                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));
                        obat.setSatuan(rs.getString("satuan"));
                        obat.setHarga_beli(rs.getString("harga_beli"));
                        obat.setHpp(rs.getString("hpp"));
                        obat.setHarga_jual(rs.getString("harga_jual"));

                    }
                    forward = "/index.jsp?halaman=edit_obat";
                    request.setAttribute("obat", obat);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);

                    break;
                case "store":
                    obat = new Obat();
                    obat.setNama(request.getParameter("nama"));
                    obat.setSatuan(request.getParameter("satuan"));
                    obat.setHarga_beli(request.getParameter("harga_beli"));
                    obat.setHpp(request.getParameter("hpp"));
                    obat.setHarga_jual(request.getParameter("harga_jual"));
                    obat.setCreate_at(date);
                    
                    
                    preparedStatement = koneksi.prepareStatement("insert into obat(nama,satuan,harga_beli,hpp,harga_jual,create_at) values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                    preparedStatement.setString(1, obat.getNama());
                    preparedStatement.setString(2, obat.getSatuan());
                    preparedStatement.setString(3, obat.getHarga_beli());
                    preparedStatement.setString(4, obat.getHpp());
                    preparedStatement.setString(5, obat.getHarga_jual());
                    preparedStatement.setString(6, obat.getCreate_at());
                    preparedStatement.executeUpdate();
                    
                    //get last id
                    rs = preparedStatement.getGeneratedKeys();
                    int obat_id=0;
                    if (rs.next()) {
                        obat_id = rs.getInt(1);
                    }  
                    
                    //masukan ke stok
                    stok = new Stok();
                    stok.setObat_id(obat_id);
                    stok.setJumlah(0);
                    stok.setCreate_at(date);
                    
                    preparedStatement = koneksi.prepareStatement("insert into stok(obat_id,jumlah,create_at) values (?,?,?)");
                    preparedStatement.setInt(1, stok.getObat_id());
                    preparedStatement.setInt(2, stok.getJumlah());
                    preparedStatement.setString(3, stok.getCreate_at());
                    preparedStatement.executeUpdate();
                    
                    
                    forward = "obat?aksi=create&status=success";
                    response.sendRedirect(forward);

                    break;
                case "update":

                    obat = new Obat();
                    obat.setNama(request.getParameter("nama"));
                    obat.setSatuan(request.getParameter("satuan"));
                    obat.setHarga_beli(request.getParameter("harga_beli"));
                    obat.setHpp(request.getParameter("hpp"));
                    obat.setHarga_jual(request.getParameter("harga_jual"));
                    obat.setUpdate_at(date);
                    obat.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi
                            .prepareStatement("update obat set nama=?,satuan=?,harga_beli=?,hpp=?,harga_jual=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setString(1, obat.getNama());
                    preparedStatement.setString(2, obat.getSatuan());
                    preparedStatement.setString(3, obat.getHarga_beli());
                    preparedStatement.setString(4, obat.getHpp());
                    preparedStatement.setString(5, obat.getHarga_jual());
                    preparedStatement.setString(6, obat.getUpdate_at());
                    preparedStatement.setInt(7, obat.getId());
                    preparedStatement.executeUpdate();

                    forward = "obat?aksi=edit&id=" + obat.getId() + "&status=success";

                    response.sendRedirect(forward);

                    break;
                case "delete":

                    obat = new Obat();
                    obat.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi
                            .prepareStatement("delete from obat where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, obat.getId());
                    preparedStatement.executeUpdate();

                    forward = "obat?status=success";

                    response.sendRedirect(forward);
                    break;
                default:

                    List<Obat> obats = new ArrayList<>();

                    Statement statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));
                        obat.setSatuan(rs.getString("satuan"));
                        obat.setHarga_beli(rs.getString("harga_beli"));
                        obat.setHpp(rs.getString("hpp"));
                        obat.setHarga_jual(rs.getString("harga_jual"));

                        obats.add(obat);
                    }

                    forward = "/index.jsp?halaman=obat";
                    request.setAttribute("obat", obats);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }
                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;

            }
            //end handle page

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ObatController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ObatController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void elseif(boolean equals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
