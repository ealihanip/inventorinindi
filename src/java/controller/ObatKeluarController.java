/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Obat;
import model.ObatKeluar;
import model.Stok;
import java.util.Date;

/**
 *
 * @author kanep
 */
public class ObatKeluarController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventori", "root", "");

            String forward = "";
            String aksi = "";
            Obat obat = new Obat();
            Stok stok = new Stok();
            ObatKeluar obatkeluar = new ObatKeluar();
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            RequestDispatcher view = null;
            Statement statement = null;
            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            List<Obat> obats = new ArrayList<>();
            List<ObatKeluar> obatkeluars = new ArrayList<>();
            int stokbaru=0;
            //jika tidak ada request di anggap index
            if (request.getParameter("aksi") != null) {

                aksi = request.getParameter("aksi");

            }

            //handle page
            switch (aksi) {

                case "create":

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));

                        obats.add(obat);
                    }

                    request.setAttribute("obat", obats);
                    forward = "/index.jsp?halaman=create_obatkeluar";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;
                case "edit":

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat");
                    while (rs.next()) {
                        obat = new Obat();
                        obat.setId(rs.getInt("id"));
                        obat.setNama(rs.getString("nama"));

                        obats.add(obat);
                    }

                    request.setAttribute("obat", obats);

                    obatkeluar = new ObatKeluar();
                    obatkeluar.setId(Integer.parseInt(request.getParameter("id")));
                    statement = koneksi.createStatement();

                    preparedStatement = koneksi.prepareStatement("select * from obat_keluar where id=?");
                    preparedStatement.setInt(1, obatkeluar.getId());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        obatkeluar.setId(rs.getInt("id"));
                        obatkeluar.setObat_id(rs.getInt("obat_id"));
                        obatkeluar.setJumlah(rs.getInt("jumlah"));
                        obatkeluar.setTanggal(rs.getString("tanggal"));
                    }

                    request.setAttribute("obatkeluar", obatkeluar);

                    forward = "/index.jsp?halaman=edit_obatkeluar";

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }

                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);

                    break;
                case "store":
                    obatkeluar = new ObatKeluar();
                    obatkeluar.setObat_id(Integer.parseInt(request.getParameter("obat_id")));
                    obatkeluar.setJumlah(Integer.parseInt(request.getParameter("jumlah")));
                    obatkeluar.setTanggal(request.getParameter("tanggal"));
                    obatkeluar.setCreate_at(date);

                    preparedStatement = koneksi.prepareStatement("insert into obat_keluar(obat_id,jumlah,tanggal,create_at) values(?,?,?,?)");
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    preparedStatement.setInt(2, obatkeluar.getJumlah());
                    preparedStatement.setString(3, obatkeluar.getTanggal());
                    preparedStatement.setString(4, obatkeluar.getCreate_at());
                    preparedStatement.executeUpdate();

                    //get data stok

                    preparedStatement = koneksi.prepareStatement("select * from stok where obat_id=?");
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        stok = new Stok();
                        stok.setId(rs.getInt("id"));
                        stok.setJumlah(rs.getInt("jumlah"));
                    }

                    //update stok

                    stokbaru=stok.getJumlah();

                    stokbaru=stokbaru-obatkeluar.getJumlah();
                    
                
                    
                    //update ke stok
                    stok.setJumlah(stokbaru);
                    stok.setUpdate_at(date);

                    preparedStatement = koneksi.prepareStatement("update stok set jumlah=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, stok.getJumlah());
                    preparedStatement.setString(2, stok.getUpdate_at());
                    preparedStatement.setInt(3, stok.getId());
                    preparedStatement.executeUpdate();
                    //end updatestok
                    

                    forward = "obatkeluar?aksi=create&status=success";
                    response.sendRedirect(forward);

                    break;
                case "update":


                    obatkeluar = new ObatKeluar();
                    obatkeluar.setId(Integer.parseInt(request.getParameter("id")));
                    statement = koneksi.createStatement();

                    preparedStatement = koneksi.prepareStatement("select * from obat_keluar where id=?");
                    preparedStatement.setInt(1, obatkeluar.getId());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        obatkeluar.setId(rs.getInt("id"));
                        obatkeluar.setObat_id(rs.getInt("obat_id"));
                        obatkeluar.setJumlah(rs.getInt("jumlah"));
                        obatkeluar.setTanggal(rs.getString("tanggal"));
                    }


                    //get data stok

                    preparedStatement = koneksi.prepareStatement("select * from stok where obat_id=?");
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        stok = new Stok();
                        stok.setId(rs.getInt("id"));
                        stok.setJumlah(rs.getInt("jumlah"));
                    }

                    //update stok

                    stokbaru=stok.getJumlah();

                    stokbaru=stokbaru+obatkeluar.getJumlah();
                
                    
                    //update ke stok
                    stok.setJumlah(stokbaru);
                    stok.setUpdate_at(date);

                    preparedStatement = koneksi.prepareStatement("update stok set jumlah=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, stok.getJumlah());
                    preparedStatement.setString(2, stok.getUpdate_at());
                    preparedStatement.setInt(3, stok.getId());
                    preparedStatement.executeUpdate();
                    //end updatestok
                    
                    
                    obatkeluar = new ObatKeluar();
                    obatkeluar.setId(Integer.parseInt(request.getParameter("id")));
                    obatkeluar.setObat_id(Integer.parseInt(request.getParameter("obat_id")));
                    obatkeluar.setJumlah(Integer.parseInt(request.getParameter("jumlah")));
                    obatkeluar.setTanggal(request.getParameter("tanggal"));
                    obatkeluar.setUpdate_at(date);

                    
                    preparedStatement = koneksi
                            .prepareStatement("update obat_keluar set obat_id=?,jumlah=?,tanggal=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    preparedStatement.setInt(2, obatkeluar.getJumlah());
                    preparedStatement.setString(3, obatkeluar.getTanggal());
                    preparedStatement.setString(4, obatkeluar.getUpdate_at());
                    preparedStatement.setInt(5, obatkeluar.getId());
                    
                    preparedStatement.executeUpdate();

                    //get data stok

                    preparedStatement = koneksi.prepareStatement("select * from stok where obat_id=?");
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        stok = new Stok();
                        stok.setId(rs.getInt("id"));
                        stok.setJumlah(rs.getInt("jumlah"));
                    }

                    //update stok

                    stokbaru=stok.getJumlah();

                    stokbaru=stokbaru-obatkeluar.getJumlah();
                
                    
                    //update ke stok
                    stok.setJumlah(stokbaru);
                    stok.setUpdate_at(date);

                    preparedStatement = koneksi.prepareStatement("update stok set jumlah=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, stok.getJumlah());
                    preparedStatement.setString(2, stok.getUpdate_at());
                    preparedStatement.setInt(3, stok.getId());
                    preparedStatement.executeUpdate();
                    //end updatestok
                    

                    forward = "obatkeluar?aksi=edit&id=" + obatkeluar.getId() + "&status=success";

                    response.sendRedirect(forward);

                    break;
                case "delete":


                     obatkeluar = new ObatKeluar();
                    obatkeluar.setId(Integer.parseInt(request.getParameter("id")));
                    statement = koneksi.createStatement();

                    preparedStatement = koneksi.prepareStatement("select * from obat_keluar where id=?");
                    preparedStatement.setInt(1, obatkeluar.getId());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        obatkeluar.setId(rs.getInt("id"));
                        obatkeluar.setObat_id(rs.getInt("obat_id"));
                        obatkeluar.setJumlah(rs.getInt("jumlah"));
                        obatkeluar.setTanggal(rs.getString("tanggal"));
                    }


                    //get data stok

                    preparedStatement = koneksi.prepareStatement("select * from stok where obat_id=?");
                    preparedStatement.setInt(1, obatkeluar.getObat_id());
                    rs = preparedStatement.executeQuery();
                    while (rs.next()) {

                        stok = new Stok();
                        stok.setId(rs.getInt("id"));
                        stok.setJumlah(rs.getInt("jumlah"));
                    }

                    //update stok

                    stokbaru=stok.getJumlah();

                    stokbaru=stokbaru+obatkeluar.getJumlah();
                
                    
                    //update ke stok
                    stok.setJumlah(stokbaru);
                    stok.setUpdate_at(date);

                    preparedStatement = koneksi.prepareStatement("update stok set jumlah=?,update_at=? where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, stok.getJumlah());
                    preparedStatement.setString(2, stok.getUpdate_at());
                    preparedStatement.setInt(3, stok.getId());
                    preparedStatement.executeUpdate();
                    //end updatestok

                    obatkeluar = new ObatKeluar();
                    obatkeluar.setId(Integer.parseInt(request.getParameter("id")));

                    preparedStatement = koneksi
                            .prepareStatement("delete from obat_keluar where id=?");
                    // Parameters start with 1
                    preparedStatement.setInt(1, obatkeluar.getId());
                    preparedStatement.executeUpdate();

                    forward = "obatkeluar?status=success";

                    response.sendRedirect(forward);
                    break;
                default:

                    statement = koneksi.createStatement();

                    rs = statement.executeQuery("select * from obat_keluar join obat on obat.id=obat_keluar.obat_id");
                    while (rs.next()) {
                        obatkeluar = new ObatKeluar();
                        obatkeluar.setId(rs.getInt("id"));
                        obatkeluar.setNama_obat(rs.getString("nama"));
                        obatkeluar.setJumlah(rs.getInt("jumlah"));
                        obatkeluar.setTanggal(rs.getString("tanggal"));
                        obatkeluars.add(obatkeluar);
                    }

                    forward = "/index.jsp?halaman=obatkeluar";
                    request.setAttribute("obatkeluar", obatkeluars);

                    if (request.getParameter("status") != null) {

                        String status = request.getParameter("status");
                        if (status.equals("success")) {

                            request.setAttribute("status", "success");
                            request.setAttribute("message", "Berhasil");

                        }
                    }
                    view = request.getRequestDispatcher(forward);
                    view.forward(request, response);
                    break;

            }
            //end handle page

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ObatController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ObatController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void elseif(boolean equals) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
