/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author kanep
 */
public class Pembelian {

    private int id;
    private int pemesanan_id;
    private String supplier;
    private String total_tagihan;
    private String jumlah_bayar;
    private String keterangan;
    private String jumlah;

    private int obat_id;
    private String nama_obat;
    private String tanggal;
    private String update_at;
    private String create_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPemesanan_id() {
        return pemesanan_id;
    }

    public void setPemesanan_id(int pemesanan_id) {
        this.pemesanan_id = pemesanan_id;
    }

    
    public String getKeterangan() {
        return keterangan;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }


    public String getSupplier() {
        return supplier;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public void setObat_id(int obat_id) {
        this.obat_id = obat_id;
    }

    public int getObat_id() {
        return obat_id;
    }

    public String getNama_obat() {
        return nama_obat;
    }

    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    
    
    public String getTotal_tagihan() {
        return total_tagihan;
    }

    public void setTotal_tagihan(String total_tagihan) {
        this.total_tagihan = total_tagihan;
    }
    public String getJumlah_bayar() {
        return jumlah_bayar;
    }

    public void setJumlah_bayar(String jumlah_bayar) {
        this.jumlah_bayar = jumlah_bayar;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    @Override
    public String toString() {
        return "";
    }

}
