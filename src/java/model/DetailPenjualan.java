/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author kanep
 */
public class DetailPenjualan {

    private int id;
    private int penjualan_id;
    private int obat_id;
    private String nama_obat;
    private String harga;
    private String tanggal;
    private int jumlah;
    private String update_at;
    private String create_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getPenjualan_id() {
        return penjualan_id;
    }

    public void setPenjualan_id(int penjualan_id) {
        this.penjualan_id = penjualan_id;
    }

    public int getObat_id() {
        return obat_id;
    }

    public void setObat_id(int obat_id) {
        this.obat_id = obat_id;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getNama_obat() {
        return nama_obat;
    }

    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.harga = tanggal;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }
    
    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    

    @Override
    public String toString() {
        return "";
    }

}
