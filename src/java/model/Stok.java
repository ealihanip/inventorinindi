/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author kanep
 */
public class Stok {

    private int id;
    private int obat_id;
    private String nama_obat;
    private int jumlah;
    private String update_at;
    private String create_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getObat_id() {
        return obat_id;
    }
    
    public void setObat_id(int obat_id) {
        this.obat_id = obat_id;
    }
    
    public String getNama_obat() {
        return nama_obat;
    }
    
    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    public int getJumlah() {
        return jumlah;
    }
    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }
    
    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "Stok [id=" + id + ", obat_id=" + obat_id + ", jumlah=" + jumlah +", create_at=" + create_at +", update_at=" + update_at + "]";
    }

}
